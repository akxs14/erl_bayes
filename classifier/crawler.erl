-module(crawler).

% -include_lib("/home/makis/yaws/lib/yaws/include/yaws_api.hrl").
-include_lib("/Users/angelos/yaws/lib/yaws/include/yaws_api.hrl").
-include_lib("page_entities.hrl").

-export([crawlUrls/1]).

%%% Crawler API

crawlUrls(Urls) ->
  save_pages(Urls),
  [ parse_page(Url,fetch_url(Url)) || Url <- Urls ],
  [{status, 201}].

%%% Private functions

%% exctacts all the text in the page and calls
%% process_text_elem/1 on each one of them
parse_page(Url, Body) ->
  page_dict:create_page_dict(Url),
  PageTree = mochiweb_html:parse(Body),
  TextElems = mochiweb_xpath:execute("//text()",PageTree),
  [ process_text_elem(Url, Elem) || Elem <- TextElems ].

fetch_url(Url) ->
  case httpc:request(binary_to_list(Url)) of
    {ok,{_,_Headers,Body}}  -> Body;
    _ -> ""
  end.

%% Save all Urls in mnesia, page table
save_pages(Urls) ->
  Fun = fun() ->
    [ mnesia:write(#page{url=Url, tags=""}) || Url <- Urls ]
  end,
  {atomic, _Res} = mnesia:transaction(Fun).

%% if it is an empty list ([]) it overlooks it,
%% otherwise it calls split_paragraphs/1 to tokenize
%% the text
process_text_elem(_Url, []) ->
  empty;
process_text_elem(Url, Elem) ->
  split_paragraph(Url, Elem).
  % io:format("~p ~n~n~n", [ets:tab2list(list_to_atom(binary_to_list(Url)))]).

split_paragraph(Url, Paragraph) ->
  [ sanitize_word(Url, Word) || Word <- re:split(Paragraph,"[ ]") ].

sanitize_word(Url, Word) ->
  UtfWord = unicode:characters_to_binary(Word,unicode,utf8),
  case re:run(UtfWord,"([-a-zA-Z\\xC4\\xD6\\xDC\\xE4\\xF6\\xFC\\xDF]+)") of
    {match, [{Start,End}, _ ] = _Res}  ->
      add_word_to_dict(Url, UtfWord, Start, End);
    _ ->
      ok
  end.  

add_word_to_dict(Url, Word, 0, End) ->
  page_dict:add_word(Url, string:substr(binary_to_list(Word), 1, End));
add_word_to_dict(Url, Word, Start, End) ->
  page_dict:add_word(Url, string:substr(binary_to_list(Word), Start + 1, End)).
