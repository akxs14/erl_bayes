-module(airport).

-include("airport.hrl").
-include("/Users/angelos/yaws/lib/yaws/include/yaws_api.hrl").
% -include("/home/makis/yaws/lib/yaws/include/yaws_api.hrl").

-export([addAirport/1, saveAirport/4, convert_to_json/1]).

% airport malakies
addAirport(Arg) ->
  {ok, Json, _} = rfc4627:decode(Arg#arg.clidata),
  Airport = rfc4627:get_field(Json, "airport", <<>>),
  City    = rfc4627:get_field(Json, "city", <<>>),
  Country = rfc4627:get_field(Json, "country", <<>>),  
  Name    = rfc4627:get_field(Json, "name", <<>>),
  _Status = saveAirport(Airport, City, Country, Name),
  [{status, 201},
   {html, Arg#arg.clidata}].

saveAirport(Code, City, Country, Name) ->
  NewRec = #airport{
    code    = Code,
    city    = City,
    country = Country,
    name    = Name },
  Add = fun() ->
      mnesia:write(NewRec)
    end,
  {atomic, _Rec} = mnesia:transaction(Add),
  NewRec.

convert_to_json(Lines) ->
  Data = [{obj,
    [{airport,  Line#airport.code},
     {city,     Line#airport.city},
     {country,  Line#airport.country},
     {name,     Line#airport.name}]}
    || Line <- Lines],
  JsonData = {obj, [{data, Data}]},
  rfc4627:encode(JsonData).