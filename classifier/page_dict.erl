-module(page_dict).

-export([create_page_dict/1, destroy_page_dict/1]).
-export([add_word/2, get_word/2]).

%% ets table with {word, frequency} as {key, value} pair
create_page_dict(Url) ->
  Table = list_to_atom(binary_to_list(Url)),
  try
    ets:delete_all_objects(Table)
  catch
    error:badarg -> 
      ets:new(Table, [set, named_table, public])
  end.

destroy_page_dict(Url) ->
  ets:destroy(list_to_atom(binary_to_list(Url))).

add_word(Url, Word) ->
  Table = list_to_atom(binary_to_list(Url)),
  update_record(ets:lookup(Table, Word), Table, Word).

update_record([], Table, Word) ->
  ets:insert(Table,{Word,1});
update_record( [{_,Frequency}], Table, Word) ->
  ets:update_element(Table, Word,{2,Frequency+1}).

get_word(Url, Word) ->
  ets:lookup(list_to_atom(binary_to_list(Url)), Word).

