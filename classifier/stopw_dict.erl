-module(stopw_dict).

-export([load_file/1, destroy_stopw_dict/0,list_all_stop_words/0, save_to_file/1]).
-export([add_to_dict/1, is_stop_word/1, init_stop_word_dict/0]).

%% Read the given file and stores in ets table stop_words
%% default file stop_words.txt (locally)
init_stop_word_dict() ->
  try
    _Info = ets:lookup(stop_words,"ok")
  catch
    error:badarg -> 
      ets:new(stop_words, [set, named_table]),
      load_file("/Users/angelos/stop_words.txt")
  end.

destroy_stopw_dict() ->
  ets:destroy(stop_words).

load_file(Filename) ->
  {ok, Device} = file:open(Filename, [read]),
  try read_lines(Device)
    after file:close(Device)
  end.

read_lines(Device) ->
  case io:get_line(Device, "") of
    eof  -> 
      [];
    Line ->  
      Word = string:substr(Line,1,string:len(Line) - 1),     
      ets:insert(stop_words, {Word, Word}),
      read_lines(Device)
  end.

list_all_stop_words() ->
  [ io:format("~p ~n", [StopWord]) || StopWord <- ets:tab2list(stop_words) ].

save_to_file(FileName) ->
  {ok, File} = file:open(FileName, [write]),
  [ io:format(File, "~s~n",[Value]) || {_Key, Value} <- ets:tab2list(stop_words) ],
  file:close(File).

add_to_dict(NewWord) ->
  ets:insert(stop_words, {NewWord, NewWord}).

is_stop_word(Word) ->
  ets:member(stop_words, Word).

