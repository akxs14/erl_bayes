curl -H "Content-Type:application/json" -d '{"airport":"TXL", "city":"Berlin", "country":"Germany", "name": "Tegel"}' http://0.0.0.0/classifier/rest.yaws/airport
curl -H "Content-Type:application/json" -d '{"airport":"JFK", "city":"Big Apple", "country":"USA", "name": "New York Int"}' http://0.0.0.0/classifier/rest.yaws/airport
curl -H "Content-Type:application/json" -d '{"airport":"LGA", "city":"Big Apple", "country":"USA", "name": "La Guardia"}' http://0.0.0.0/classifier/rest.yaws/airport
curl -H "Content-Type:application/json" -d '{"airport":"CDG", "city":"Paris", "country":"France", "name": "Charles De Gaulle"}' http://0.0.0.0/classifier/rest.yaws/airport
